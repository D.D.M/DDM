import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

public class MyMenuItem {
    JMenuItem menuItem;
    public MyMenuItem(String name) {
        menuItem = new JMenuItem(name);
        menuItem.setName(name);
        menuItem.addActionListener(new Action());
    }

    public MyMenuItem(String name,int press, int hold,int mmemonic) {
        menuItem = new JMenuItem(name);
        menuItem.setName(name);
        setMenuItem(press, hold,mmemonic);
        menuItem.addActionListener(new Action());
    }

    public JMenuItem getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(int press, int hold,int mmemonic) {
        menuItem.setAccelerator(KeyStroke.getKeyStroke(press, hold));
        menuItem.setMnemonic(mmemonic);
    }

    private class Action implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(menuItem.getName().equals("Exit")) {
               System.exit(0);
            }
            if(menuItem.getName().equals("New DownloadInfo")) {
//                UiManager.frames.newDownload.getNewDownload().setVisible(true);
                NewDownload newDownload = new NewDownload();
                UiManager.frames.newDownload = newDownload;
                newDownload.setComboBoxItems(UiManager.queue.getStrings());
                UiManager.frames.newDownload.getNewDownload().setVisible(true);
            }
            if(menuItem.getName().equals("Setting")) {
                UiManager.frames.setting.getSetting().setVisible(true);
            }
            if(menuItem.getName().equals("About Us")){
                UiManager.frames.getAbout().setVisible(true);
            }
        }
    }
}
