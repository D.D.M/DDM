import java.io.*;
import java.util.ArrayList;

public class FileUtils {
    private static  File downloadInfo;
    private static  File settingInfo;
    private static File file;

    public FileUtils(){

        if(settingInfo == null)
    settingInfo = new File("Setting Info.ddm");
        if(downloadInfo == null)
            downloadInfo = new File("list.ddm");
        if(file == null)
            file = new File("file.ddm");
    }
    public static void addDownload(ArrayList<Download>download){
        try(FileOutputStream fileOutputStream = new FileOutputStream(downloadInfo.getPath())) {
            try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
                for (int i=0;i<download.size();i++)
                objectOutputStream.writeObject(download.get(i).getDownloadInfo());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static ArrayList<Download> getdownloads(){
        ArrayList<Download>downloads = new ArrayList<Download>();
        if(downloadInfo.exists()) {
            try (FileInputStream fileInputStream = new FileInputStream(downloadInfo.getPath())) {
                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

                DownloadInfo download = (DownloadInfo) objectInputStream.readObject();

                while (download != null) {
                    Download downloading = new Download(download);
                    downloads.add(downloading);
                    System.out.println(download.getName());
                    download = (DownloadInfo) objectInputStream.readObject();
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return downloads;
    }
//    public static UiManager getUi(){
//        if(file.exists()) {
//            try (FileInputStream fileInputStream = new FileInputStream(file.getPath())) {
//                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
//
//                UiManager uiManager = (UiManager) objectInputStream.readObject();
//                return uiManager;
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (ClassNotFoundException e) {
//                e.printStackTrace();
//            }
//        }
//        return null;
//    }
//    public static void save(UiManager uiManager){
//        try(FileOutputStream fileOutputStream = new FileOutputStream(file.getPath())) {
//            try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
//                    objectOutputStream.writeObject(uiManager);
//            }
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
    public static void getSetting(Setting setting){
        if(settingInfo.exists()) {
            try (FileInputStream fileInputStream = new FileInputStream(settingInfo.getPath())) {
                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

                setting.setAddress((String) objectInputStream.readObject());
                setting.setLookAndFeel((String)objectInputStream.readObject());
                setting.setL((ArrayList<String>)objectInputStream.readObject());

                    //setting = (Setting) objectInputStream.readObject();

            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

    }
    public static void saveSetting(Setting setting){
        try(FileOutputStream fileOutputStream = new FileOutputStream(settingInfo.getPath())) {
            try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
                    objectOutputStream.writeObject(setting.getAddress());
                    objectOutputStream.writeObject(setting.getLookAndFeel());
                    ArrayList<String>strings = new ArrayList<String>();
                    for (int i=0;i<setting.getButtons().size();i++)
                    strings.add(setting.getButtons().get(i).getName());
                    objectOutputStream.writeObject(strings);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
