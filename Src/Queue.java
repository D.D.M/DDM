import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Queue {

    private static Tab tab;
    private ArrayList<ProgressBar>progressBars;
    private ArrayList<JPanel>panels;
    private ArrayList<String> strings;
    private ArrayList<Download> downloads;
    private JPanel defaultpanel;
    private JList list;
    private String sort;
    private JScrollPane scrollPane;
    private int hamzaman;
    private int count;
    private boolean sortnig;
    private  File zipFile;
    public Queue(){
        count = 1;
        zipFile = new File("D:\\IdeaProjects\\DownloadManager\\DDM\\DDM\\export.zip");
        hamzaman = 1;
        sortnig = false;
        sort = "name";
        panels = new ArrayList<>();
        tab = new Tab();
        strings = new ArrayList<>();
        progressBars = new ArrayList<>();
        downloads = new ArrayList<Download>();
    }

    public void setHamzaman(int hamzaman) {
        this.hamzaman = hamzaman;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Queue(Tab tab, JPanel defaultPanel, JScrollPane scrollPane){
        count = 1;
        zipFile = new File("D:\\IdeaProjects\\DownloadManager\\DDM\\DDM\\export.zip");
        hamzaman = 1;
        sortnig = false;
        sort = "name";
        panels = new ArrayList<>();
        tab = new Tab();
        strings = new ArrayList<>();
        progressBars = new ArrayList<>();
        downloads = new ArrayList<Download>();
        this.tab = tab;
        strings = new ArrayList<>();
        panels = new ArrayList<>();
        progressBars = new ArrayList<>();
        this.defaultpanel = defaultPanel;
        downloads = new ArrayList<Download>();
        this.scrollPane = scrollPane;
    }

    public boolean isSortnig() {
        return sortnig;
    }

    public void setSortnig(boolean sortnig) {
        this.sortnig = sortnig;
    }

    public void setQueues(String name, Download download){
        ProgressBar progressBar = new ProgressBar(download);
        download.addProgressBar(progressBar);
        if(!name.equals("Processing")) {
            strings.add(name);
            JPanel panel = new JPanel(new GridLayout(10, 1));
            panel.setName(name);
            panels.add(panel);
//            panel.add(progressBar.getProgressBar());TODO
            panel.add(progressBar.getPanel());
            panel.setVisible(true);
            panel.setName(name);
            tab.add(name, panel);
            tab.getTabbedPane().setTabPlacement(SwingConstants.LEFT);
        }
        LookAndFeel.updateLAFRecursively(UiManager.mainFrame);
        //    tab.getTabbedPane().updateUI();
//    tab.getTabbedPane().revalidate();
//    tab.getTabbedPane().repaint();
//    queues.updateUI();
//    queues.revalidate();
//    queues.repaint();
//    LookAndFeel.updateLAFRecursively(mainFrame);
    }
    public void add(String name, Download download) {
        boolean flag = false;

        if (download.getDownloadInfo().isRemoved())
            return;
        ProgressBar progressBar = new ProgressBar(download);
        download.addProgressBar(progressBar);
        progressBars.add(progressBar);

//        defaultpanel.add(progressBar.getProgressBar());TODO
//        scrollPane.add(defaultpanel);
        defaultpanel.add(progressBar.getPanel());
        for (int i = 0; i < panels.size(); i++) {
            if (panels.get(i).getName().equals(name)) {
                ProgressBar progressBar1 = new ProgressBar(download);
                panels.get(i).add(progressBar1.getProgressBar());
                download.addProgressBar(progressBar1);
                flag = true;
                break;
            }
        }
        downloads.add(download);
        if (!flag) {
            setQueues(name, download);
        }

    }

    public ArrayList<JPanel> getPanels() {
        return panels;
    }

    public ArrayList<ProgressBar> getProgressBars() {
        return progressBars;
    }

    public ArrayList<String> getStrings() {
        return strings;
    }
    public void readFile(){
        ArrayList<Download> downloads = FileUtils.getdownloads();
//        System.out.println(downloads.get(0).getDownloadInfo().getValue());
        if(downloads.size() != 0) {
            for (int i = 0; i < downloads.size(); i++) {
                add(downloads.get(i).getDownloadInfo().getQueue(), downloads.get(i));
            }
        }
    }

    public ArrayList<Download> getDownloads() {
        return downloads;
    }

    public JPanel getDefaultpanel() {
        return defaultpanel;
    }

    public static Tab getTab() {
        return tab;
    }
//    public void update(){
//        defaultpanel.removeAll();
//        for (int i=0;i<=panels.size();i++){
//        panels.remove(0);
//        i=0;
//        }
//        for (int i=0;i<=progressBars.size();i++){
//            progressBars.remove(0);
//            i=0;
//        }
//        ArrayList<DownloadInfo>downloads = new ArrayList<DownloadInfo>();
//        for (int i=0;i<this.downloads.size();i++){
//            downloads.add(this.downloads.get(i));
//        }
//        Iterator iterator = downloads.iterator();
//        while (iterator.hasNext()){
//            iterator.next();
//            iterator.remove();
//        }
//        System.out.println(downloads.size());
//        System.out.println(panels.size());
//        System.out.println(progressBars.size());
//            for (int i = 0; i < downloads.size(); i++) {
//                add(downloads.get(i).getQueue(), downloads.get(i));
//            }
//    }
public void setSort(String sort) {
    this.sort = sort;
    sorting();
}

    public String getSort() {
        return sort;
    }

    public void sorting(){
        if(sort.equals("name")) {
            for (int j=0;j<downloads.size();j++)
            for (int i = 0; i <downloads.size();i++){
                if(sortnig) {
                    if (downloads.get(j).getDownloadInfo().getName().compareTo(downloads.get(i).getDownloadInfo().getName()) < 0) {
                        Download temp = downloads.get(i);
                        downloads.set(i, downloads.get(j));
                        downloads.set(j, temp);
                    }
                }
                else {
                    if (downloads.get(j).getDownloadInfo().getName().compareTo(downloads.get(i).getDownloadInfo().getName()) > 0) {
                        Download temp = downloads.get(i);
                        downloads.set(i, downloads.get(j));
                        downloads.set(j, temp);
                    }
                }

            }
            defaultpanel.removeAll();
            tab.getTabbedPane().removeAll();
        }
        if(sort.equals("size")) {
            for (int j=0;j<downloads.size();j++)
                for (int i = 0; i <downloads.size();i++){
                if(sortnig) {
                    if (downloads.get(j).getDownloadInfo().getSize() < downloads.get(i).getDownloadInfo().getSize()) {
                        Download temp = downloads.get(i);
                        downloads.set(i, downloads.get(j));
                        downloads.set(j, temp);
                    }
                }
                else {
                    if (downloads.get(j).getDownloadInfo().getSize() > downloads.get(i).getDownloadInfo().getSize()) {
                        Download temp = downloads.get(i);
                        downloads.set(i, downloads.get(j));
                        downloads.set(j, temp);
                    }
                }
                }
            defaultpanel.removeAll();
            tab.getTabbedPane().removeAll();
        }
        //TODO
        if(sort.equals("calender")) {

                for (int j = 0; j < downloads.size(); j++)
                    for (int i = 0; i < downloads.size(); i++) {
                        if(sortnig) {
                        if (downloads.get(j).getDownloadInfo().getDate().compareTo(downloads.get(i).getDownloadInfo().getDate()) < 0) {
                            Download temp = downloads.get(i);
                            downloads.set(i, downloads.get(j));
                            downloads.set(j, temp);
                        }
                    }
            else {
                if (downloads.get(j).getDownloadInfo().getDate().compareTo(downloads.get(i).getDownloadInfo().getDate()) > 0) {
                    Download temp = downloads.get(i);
                    downloads.set(i, downloads.get(j));
                    downloads.set(j, temp);
                }
            }
            }
            defaultpanel.removeAll();
            tab.getTabbedPane().removeAll();
        }

        progressBars = new ArrayList<ProgressBar>();
        panels = new ArrayList<JPanel>();

        for (int i=0;i<downloads.size();i++) {
            System.out.println(downloads.get(i).getDownloadInfo().getName());
            boolean flag = false;
            if (!downloads.get(i).getDownloadInfo().isRemoved()) {
                ProgressBar progressBar = new ProgressBar(downloads.get(i));
                progressBars.add(progressBar);
//                defaultpanel.add(progressBar.getProgressBar()); TODO
                defaultpanel.add(progressBar.getPanel());
                for (int k=0;k<panels.size();k++){
                    if (panels.get(k).getName().equals(downloads.get(i).getDownloadInfo().getQueue())) {
//                        panels.get(k).add(progressBar.getProgressBar());TODO
                        panels.get(k).add(progressBar.getPanel());
                        flag = true;
                        break;
                    }
                }
                if (!flag)
                    setQueues(downloads.get(i).getDownloadInfo().getQueue(), downloads.get(i));
            }
        }
    }
    public void removed(){
        JFrame frame = new JFrame();
        int count = 0;
        for (int i=0;i<downloads.size();i++){
            if(downloads.get(i).getDownloadInfo().isRemoved())
                count++;
        }
        JPanel panel = new JPanel(new GridLayout(count+1,1));
        for (int i=0;i<downloads.size();i++){
            if(downloads.get(i).getDownloadInfo().isRemoved()){
                ProgressBar progressBar = new ProgressBar(downloads.get(i));
                panel.add(progressBar.getProgressBar());
            }
        }
        JButton button = new JButton("OK");
        ActionListener actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(e.getSource().equals(button)){
                    frame.setVisible(false);
                }
            }
        };
        panel.add(button);
        button.addActionListener(actionListener);
        frame.setContentPane(panel);
        frame.pack();
        frame.setVisible(true);
    }
    public void search(String name,JPanel panel){
        panel.removeAll();
        for (int i=0;i<downloads.size();i++){
            if(downloads.get(i).getDownloadInfo().getName().contains(name)){
                ProgressBar progressBar = new ProgressBar(downloads.get(i));
                panel.add(progressBar.getProgressBar());
            }
        }
        LookAndFeel.updateLAFRecursively(UiManager.mainFrame);
        panel.updateUI();
        panel.revalidate();
        panel.repaint();
    }
    public void startQueue(String queue){
        int count = 1;
        for(int i=0;i<downloads.size();i++){
            if(count <= 1)
          if(downloads.get(i).getDownloadInfo().getQueue().equals(queue) && !downloads.get(i).getDownloadInfo().isComplete()){
                downloads.get(i).setQueue(true);
              downloads.get(i).execute();
              count++;
          }
        }
    }

    public int getHamzaman() {
        return hamzaman;
    }

    public void test(int i){
        try {
            downloads.get(i).execute();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void addToZip(Download download)  {
        try {
            fileToZip(download.getDownloadFile(),5);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private  void fileToZip(File file, int compressionLevel) throws Exception {

//        zipFile.createNewFile();
        FileOutputStream fout = new FileOutputStream(zipFile);
        ZipOutputStream zout = null;
        try {
            zout = new ZipOutputStream(new BufferedOutputStream(fout));
            zout.setLevel(compressionLevel);
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                for (int i = 0; i < files.length; i++)
                    fileToZip(files[i], zout, file);
            } else if (file.isFile()) {
                fileToZip(file, zout, file.getParentFile());
            }
        } finally {
            if (zout != null)
                zout.close();
        }
    }

    private  void fileToZip(File file, ZipOutputStream zout, File baseDir) throws Exception {
        String entryName = file.getPath().substring(baseDir.getPath().length() + 1);
        if (File.separatorChar != '/')
            entryName = entryName.replace(File.separator, "/");
        if (file.isDirectory()) {
            zout.putNextEntry(new ZipEntry(entryName + "/"));
            zout.closeEntry();
            File[] files = file.listFiles();
            for (int i = 0; i < files.length; i++)
                fileToZip(files[i], zout, baseDir);
        } else {
            FileInputStream is = null;
            try {
                is = new FileInputStream(file);
                zout.putNextEntry(new ZipEntry(entryName));
                streamCopy(is, zout);
            } finally {
                zout.closeEntry();
                if (is != null)
                    is.close();
            }
        }
    }

    private  void streamCopy(InputStream is, OutputStream os) throws IOException {
        byte[] buffer = new byte[32768];
        int len;
        while ((len = is.read(buffer)) > 0) {
            os.write(buffer, 0, len);
        }
    }
}
