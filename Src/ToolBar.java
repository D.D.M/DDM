import javax.swing.*;

public class ToolBar {
    private JToolBar toolBar;
    public ToolBar(String in){
        if(in == null)
        toolBar = new JToolBar();

       // toolBar.setSize(100,500);
        if(in.equals("vertical")) {

            toolBar = new JToolBar(null, JToolBar.VERTICAL);
        }
        toolBar.setFloatable(false);
    }
    public ToolBar(){
            toolBar = new JToolBar();
        // toolBar.setSize(100,500);
    }
    public JToolBar getToolBar() {
        return toolBar;
    }
    public void addButton(MyButton button){
        toolBar.add(button.getButton());
    }

}
