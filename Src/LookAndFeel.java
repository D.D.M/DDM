import javax.swing.*;
import java.awt.*;

public  class LookAndFeel {
    public  LookAndFeel(String in){
    setLookAndFeel(in);
    }
    public static void setLookAndFeel(String in){
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info :  javax.swing.UIManager.getInstalledLookAndFeels()) {
                if (in.equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    updateLAFRecursively(UiManager.mainFrame);
                    updateLAFRecursively(UiManager.frames.newDownload.getNewDownload());
                    updateLAFRecursively(UiManager.frames.setting.getSetting());
                    updateLAFRecursively(UiManager.frames.getAbout());
                    break;
                }
            }
//        } catch (ClassNotFoundException | InstantiationException || javax.swing.UnsupportedLookAndFeelException ex) {
        } catch (Exception ex) {
//            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
    public static void updateLAFRecursively(Window window) {
        for (Window childWindow : window.getOwnedWindows()) {
            updateLAFRecursively(childWindow);
        }
        SwingUtilities.updateComponentTreeUI(window);
    }
}
