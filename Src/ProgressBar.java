import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;

public class ProgressBar {
    private JProgressBar progressBar;
    private String url;
    private String name;
    private String address;
    private String type;
    private  JPanel panel;
    private JLabel label;
    private JList list;
    private String queue;
    private boolean selected;
    private Download download;
    public ProgressBar(){
        selected = false;
        panel = new JPanel();
        panel.setBorder(new EmptyBorder(3,3,3,3));
        label = new JLabel();
        progressBar = new JProgressBar();
        Border border = BorderFactory.createTitledBorder("DOWNLOADING...");
        progressBar.setBorder(border);
        progressBar.setStringPainted(true);
       // progressBar.setBackground(Color.cyan);
        progressBar.setForeground(Color.blue);
        progressBar.setOpaque(true);
        progressBar.addMouseListener(new Mouse());
        progressBar.setBounds(110,20,200,25);
        progressBar.setBackground(Color.red);
        label.add(progressBar);
        panel.add(label);
    }
    public ProgressBar(Download download){
        selected = false;
        ImageIcon loading = new ImageIcon("Src\\5dgr.gif");
        panel = new JPanel();
        panel.setLayout(new BorderLayout(3,3));
        panel.setBorder(new EmptyBorder(3,3,3,3));
        label = new JLabel("loading... ", loading, JLabel.CENTER);
        progressBar = new JProgressBar(0,100);
//        Border border = BorderFactory.createTitledBorder("DOWNLOADING...");
//        progressBar.setBorder(border);
        progressBar.setBorder(new BevelBorder(BevelBorder.RAISED));
//        progressBar.setBorder(new LineBorder(Color.black, 5));
//        progressBar.setBorder(new EtchedBorder(EtchedBorder.RAISED));
//        progressBar.setBorder(new SoftBevelBorder(SoftBevelBorder.LOWERED));
//        progressBar.setBorder(new MatteBorder(new ImageIcon("Src\\5.gif")));
//        progressBar.setBorder(new TitledBorder(new MatteBorder(new ImageIcon("Srcl\\5.gif")), "Title String"));
//        progressBar.setBorder(new TitledBorder(new LineBorder(Color.black, 5), "Title String"));
//        progressBar.setBorder(new TitledBorder(new EmptyBorder(10, 10, 10, 10), "Title String"));

        progressBar.setStringPainted(true);
        progressBar.addMouseListener(new Mouse());
        setUrl(download.getDownloadInfo().getUrl());
        setName(download.getDownloadInfo().getName());
        setAddress(download.getDownloadInfo().getAddress());
        setType(download.getDownloadInfo().getType());
        setValue(download.getDownloadInfo().getValue());
        setQueue(download.getDownloadInfo().getQueue());
        progressBar.setForeground(Color.blue);
//        progressBar.paintImmediately(100,100,100,100);

        progressBar.setBackground(Color.black);
//        progressBar.setDebugGraphicsOptions(5);
      //  label.setBorder(new TitledBorder(new MatteBorder(new ImageIcon("Src\\5.gif")), "Title String"));
        JPanel panel1 = new JPanel(new GridLayout(1,2));
        JPanel panel2 = new JPanel(new GridLayout(1,2));
        JLabel label1 = new JLabel();
        JLabel label2 = new JLabel();
        JLabel label3 = new JLabel();
        label2.setText("" + download.getDownloadInfo().getSize());
        label1.setText(download.getDownloadInfo().getDate().toString());
        label3.setText(download.getDownloadInfo().getName());
//        label1.setBackground(Color.YELLOW);
//        label3.setBackground(Color.YELLOW);
//        label2.setBackground(Color.YELLOW);
        label1.setBorder(new TitledBorder(new LineBorder(Color.black, 1), "Time : "));
        label3.setBorder(new TitledBorder(new LineBorder(Color.black, 1), "Name : "));
        label2.setBorder(new TitledBorder(new LineBorder(Color.black, 1), "Size : "));
        panel1.add(label1);
        panel1.add(label2);
        panel2.add(label3);
        panel2.add(label);
        panel.setBackground(Color.black);
        panel.add(progressBar,BorderLayout.CENTER);
        panel.add(panel1,BorderLayout.SOUTH);
        panel.add(panel2,BorderLayout.NORTH);
        this.download = download;
    }
    public void setValue(float value){
        progressBar.setValue((int)value);
    }
    public void add(String string){
        progressBar.setString(string);
    }
    public JProgressBar getProgressBar() {
        return progressBar;
    }
    public void updateLable(float in){
        label.setText("" + in);
        label.setBorder(new TitledBorder(new LineBorder(Color.black, 1), "Speed : "));
    }
    public int getValue(){
        return progressBar.getValue();
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getQueue() {
        return queue;
    }

    public void updateValue(int count){
        progressBar.setValue(progressBar.getValue()+count);
        }
    public boolean isSelected() {
        return selected;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public JLabel getLabel() {
        return label;
    }

    public JPanel getPanel() {
        return panel;
    }
    public void setMax(int  in){
        progressBar.setMaximum(in);
    }
    public Download getDownload() {
        return download;
    }

    public void showInformation(){
        JFrame frame = new JFrame("Information");
        frame.setName("Information");
        JPanel panel = new JPanel(new BorderLayout(5,5));
        panel.setBorder(new EmptyBorder(5,5,5,5));
        JLabel label = new JLabel("DownloadInfo file INFORMATION");
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setBackground(Color.ORANGE);
        label.setOpaque(true);

        JLabel url = new JLabel("URL : " + this.url);
        url.setHorizontalAlignment(SwingConstants.CENTER);
        url.setBackground(Color.YELLOW);
        url.setOpaque(true);

        JLabel name  = new JLabel("NAME : " + this.name);
        name.setHorizontalAlignment(SwingConstants.CENTER);
        name.setBackground(Color.YELLOW);
        name.setOpaque(true);

        JLabel size = new JLabel("size : " );
        size.setHorizontalAlignment(SwingConstants.CENTER);
        size.setBackground(Color.blue);
        size.setOpaque(true);

        JLabel type = new JLabel("type : " );
        type.setHorizontalAlignment(SwingConstants.CENTER);
        type.setBackground(Color.cyan);
        type.setOpaque(true);

        JLabel address = new JLabel("address : " + this.address);
        address.setHorizontalAlignment(SwingConstants.CENTER);
        address.setBackground(Color.pink);
        address.setOpaque(true);

        MyButton button = new MyButton("OK",frame);

        JPanel panel1 = new JPanel(new GridLayout(6,1,5,5));
        panel1.add(url);
        panel1.add(name);
        panel1.add(address);

        JButton myButton = new JButton("Open folder");
        myButton.addMouseListener(new Mouse());
        panel1.add(myButton);
        panel1.add(size);
        panel1.add(type);

        panel.add(label,BorderLayout.NORTH);
        panel.add(panel1,BorderLayout.CENTER);
        panel.add(button.getButton(),BorderLayout.SOUTH);
        frame.setContentPane(panel);
        frame.setVisible(true);
        frame.setSize(new Dimension(400,500));
        frame.setLocation(700,300);
        frame.setResizable(false);
//        frame.pack();
    }

    private class Mouse implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {

        }

        @Override
        public void mousePressed(MouseEvent e) {
            File file = new File(address + "\\"+ name);
            if (e.getClickCount() == 2 && e.getButton() == 1 ) {
                if (file.exists() && progressBar.getValue() == 100) {
                    try {
                        Desktop.getDesktop().open(file);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                } else {
                    file = new File(address);
                    if (file.exists()) {
                        try {
                            Desktop.getDesktop().open(file);
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            }
            if (e.getClickCount() == 1 && e.getButton() == 1){
                selected = !selected;
                if(selected) {
                    progressBar.setBackground(Color.blue);
                    panel.setBackground(Color.blue);
                }
                else {
                    progressBar.setBackground(Color.black);
                    panel.setBackground(Color.black);
                }
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {

            if (e.getButton() == 3) {
                showInformation();
            }

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }
    }

