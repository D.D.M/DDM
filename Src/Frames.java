import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.Serializable;
import java.util.ArrayList;

public class Frames implements Serializable{
    public NewDownload newDownload;
    public Setting setting;
    private JFrame about;
    private JFrame queue;

    public Frames(){
        setting = new Setting();
        FileUtils.getSetting(setting);
        LookAndFeel.setLookAndFeel(setting.getLookAndFeel());
        about();
    }



    public void about(){
        about = new JFrame("ABOUT US");
        about.setName("ABOUT US");
        JPanel panel = new JPanel(new BorderLayout(5,5));
        JLabel label = new JLabel();
        label.setIcon(new ImageIcon("D:\\IdeaProjects\\DownloadManager\\DDM\\DDM\\Src\\about.jpg"));
        about.setResizable(false);
        about.setContentPane(panel);
        MyButton button = new MyButton("OK",about);
        MyButton button1 = new MyButton("cancel",about);
        panel.add(label,BorderLayout.CENTER);
        JPanel panel1 = new JPanel(new BorderLayout(5,5));
        panel1.add(button.getButton(),BorderLayout.WEST);
        panel1.add(button1.getButton(),BorderLayout.EAST);
        panel.add(panel1,BorderLayout.SOUTH);
        about.pack();
    }
    public void queueFrame(){
        //TODO
        if(queue != null){
            queue.pack();
            queue.setVisible(true);
            return;
        }
        queue = new JFrame();
        JPanel panel = new JPanel(new GridLayout(2,2));
        panel.setBorder(new EmptyBorder(5,5,5,5));
//        ArrayList<JTextField> textField = new ArrayList<JTextField>;
//        JPopupMenu popupMenu = new JPopupMenu();
//        ArrayList<JMenuItem> menuItems = new ArrayList<JMenuItem>();
//        for(int i=0;i<10;i++){
//            menuItems.add(new JMenuItem("" + i));
//            menuItems.get(i).setName("" + i);
//            String name = menuItems.get(i).getName();
//            menuItems.get(i).addActionListener(new ActionListener() {
//                @Override
//                public void actionPerformed(ActionEvent e) {
//                    UiManager.queue.setHamzaman(Integer.parseInt(name));
//                    popupMenu.setVisible(false);
//                }
//            });
//        }
////        for (int i=0;i<UiManager.queue.getPanels().size();i++){
////            menuItems.add(new MenuItem(UiManager.queue.getPanels().get(i).getName()));
////            textField.add(new JTextField());
////        }
////        ActionListener actionListener new ActionListener() {
////            @Override
////            public void actionPerformed(ActionEvent e) {
////
////            }
////        }
//        for (int i=0;i<menuItems.size();i++){
//            popupMenu.add(menuItems.get(i));
//        }
//        JButton button = new JButton("set");
//        button.addMouseListener(new MouseListener() {
//            @Override
//            public void mouseClicked(MouseEvent e) {
//
//            }
//
//            @Override
//            public void mousePressed(MouseEvent e) {
//
//            }
//
//            @Override
//            public void mouseReleased(MouseEvent e) {
//                popupMenu.setVisible(true);
//            }
//
//            @Override
//            public void mouseEntered(MouseEvent e) {
//
//            }
//
//            @Override
//            public void mouseExited(MouseEvent e) {
//
//            }
//        });
//        button.add(popupMenu);
//        panel.add(button);


        JTextField textField = new JTextField();
        JButton button = new JButton("ok");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UiManager.queue.setHamzaman(Integer.parseInt(textField.getText()));
            }
        });
        JButton start = new JButton("Start");
        JTextField textField1 = new JTextField();
        start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UiManager.queue.startQueue(textField1.getText());
            }
        });
        panel.add(textField);
        panel.add(button);
        panel.add(textField1);
        panel.add(start);
        queue.setContentPane(panel);
        queue.pack();
        queue.setVisible(true);
    }
    public JFrame getAbout() {
        return about;
    }

}
