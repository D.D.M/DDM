import com.sun.xml.internal.ws.api.config.management.policy.ManagementAssertion;

import javax.swing.*;
import java.awt.event.*;

public class TextField {
    private JTextField textField;
    private JFrame currentFrame;
    private String text;
    public TextField(String string,String name, JFrame frame){
        textField = new JTextField(string);
        textField.setName(name);
        currentFrame = frame;
        textField.addActionListener(new Action());
        textField.addKeyListener(new Key());
        textField.addMouseListener(new Mouse());
    }
    public TextField(){
        textField = new JTextField();
    }
    public JTextField getTextField() {
        return textField;
    }
    private class Action implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            text = textField.getText();
            if(textField.getName().equals("name") && currentFrame.getName().equals("New DownloadInfo")){
                if(textField.getText().equals("Type here your FILE NAME")){
                    getTextField().setText("");
                }
            }
            if(textField.getName().equals("URL") && currentFrame.getName().equals("New DownloadInfo")){
                if(textField.getText().equals("Type here your destination address") || !textField.getText().startsWith("Https://") || !textField.getText().startsWith("Http://")){
                    getTextField().setText("Https://");
                }
            }

        }
    }
    private class Key implements KeyListener{

        @Override
        public void keyTyped(KeyEvent e) {

        }

        @Override
        public void keyPressed(KeyEvent e) {
            if(textField.getName().equals("name") && currentFrame.getName().equals("New DownloadInfo")){
                if(textField.getText().equals("Type here your FILE NAME")){
                    getTextField().setText("");
                }
            }
            if(textField.getName().equals("URL") && currentFrame.getName().equals("New DownloadInfo")){
                if(textField.getText().equals("Type here your destination address") || !textField.getText().startsWith("Https://")){
                    getTextField().setText("Https://");
                }
            }
            text = textField.getText();
        }

        @Override
        public void keyReleased(KeyEvent e) {

        }
    }
    private class Mouse implements MouseListener{

        @Override
        public void mouseClicked(MouseEvent e) {
            text = textField.getText();
            if(textField.getName().equals("name") && currentFrame.getName().equals("New DownloadInfo")){
                if(textField.getText().equals("Type here your FILE NAME")){
                    getTextField().setText("");
                }
            }
            if(textField.getName().equals("URL") && currentFrame.getName().equals("New DownloadInfo")){
                if(textField.getText().equals("Type here your destination address") || !textField.getText().startsWith("Https://")){
                    getTextField().setText("Https://");
                }
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }
}
