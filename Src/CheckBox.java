import javax.swing.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class CheckBox extends JCheckBox {
private JCheckBox checkBox;
public CheckBox(String name){
    checkBox = new JCheckBox(name);
    checkBox.setName(name);
    checkBox.addItemListener(new Item());
}

    public JCheckBox getCheckBox() {
        return checkBox;
    }

    private class Item implements ItemListener{

    @Override
    public void itemStateChanged(ItemEvent e) {

    }
}
}
