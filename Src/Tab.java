import javax.swing.*;
import java.awt.*;

public class Tab {
    private JTabbedPane tabbedPane;
    public Tab(){
        tabbedPane = new JTabbedPane();
    }
    public void add(String name,JPanel panel){
        tabbedPane.add(name,panel);
    }

    public void add(String name, String address,JPanel panel){
        tabbedPane.add(name,panel);
        tabbedPane.setIconAt(tabbedPane.getTabCount()-1,new ImageIcon(address));

    }
    public void add(String name,MyButton button){
        tabbedPane.addTab(name,button.getButton());


    }
    public JTabbedPane getTabbedPane() {
        return tabbedPane;
    }
}
