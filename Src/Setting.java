import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

public class Setting implements Serializable {
    private JFrame setting;
    private String address;
    private JPanel filter;
//    private JList<String>settingJList;
//    private  DefaultListModel<String> l;
    private ArrayList<JButton> buttons;
    private String lookAndFeel;
    private JPanel panel1;
    public  Setting() {
        if(buttons == null)
        buttons = new ArrayList<JButton>();
        lookAndFeel = "Nimbus";
        address = "sfaaf";
        try {
            address = new java.io.File( "." ).getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        setting = new JFrame("Setting");
        setting.setName("Setting");
        setting.setMinimumSize(new Dimension(400,200));
        setting.setResizable(false);
        setting.add(new JPanel());
        Tab tab = new Tab();
        JFileChooser chooser = new JFileChooser();

        JPanel chooserFrame = new JPanel();


        JRadioButton option1 = new JRadioButton("Metal");
        JRadioButton option2 = new JRadioButton("Nimbus");
        JRadioButton option3 = new JRadioButton("CDE/Motif");
        JRadioButton option4 = new JRadioButton("Windows");
        JRadioButton option5 = new JRadioButton("Windows Classic");

        filter = new JPanel(new BorderLayout(5,5));//TODO
//        l = new DefaultListModel<>();
//        l.addElement("Filtering sites : ");
//        settingJList = new JList<String>(l);
        JTextField textField = new JTextField();
        JButton addFilter = new JButton("OK");
        JPanel panel = new JPanel(new GridLayout(4,1));
        JButton remove = new JButton("Remove");
        panel.add(textField);
        panel.add(addFilter);
        panel.add(remove);
        if(panel1 == null)
         panel1 = new JPanel(new GridLayout(10,1));
        filter.add(panel,BorderLayout.CENTER);
        filter.add(panel1,BorderLayout.EAST);
        ActionListener actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (e.getSource().equals(option1)) {
                    LookAndFeel.setLookAndFeel("Metal");
                    lookAndFeel = "Metal";
                    Object[] options = {"ok"};
                    JOptionPane.showOptionDialog(null, "SUCCESSFULY Changed :)", "Information", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
                }
                if (e.getSource().equals(option2)) {
                    LookAndFeel.setLookAndFeel("Nimbus");
                    lookAndFeel = "Nimbus";
                    Object[] options = {"ok"};
                    JOptionPane.showOptionDialog(null, "SUCCESSFULY Changed :)", "Information", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
                }
                if (e.getSource().equals(option3)) {
                    LookAndFeel.setLookAndFeel("CDE/Motif");
                    lookAndFeel = "CDE/Motif";
                    Object[] options = {"ok"};
                    JOptionPane.showOptionDialog(null, "SUCCESSFULY Changed :)", "Information", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
                }
                if (e.getSource().equals(option4)) {
                    LookAndFeel.setLookAndFeel("Windows");
                    lookAndFeel = "Windows";
                    Object[] options = {"ok"};
                    JOptionPane.showOptionDialog(null, "SUCCESSFULY Changed :)", "Information", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);

                }
                if (e.getSource().equals(option5)) {
                    LookAndFeel.setLookAndFeel("Windows Classic");
                    lookAndFeel = "Windows Classic";
                    Object[] options = {"ok"};
                    JOptionPane.showOptionDialog(null, "SUCCESSFULY Changed :)", "Information", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
                }
                if(e.getSource().equals(addFilter)) {
                    if (textField.getText() != null) {
                        JButton button = new JButton(textField.getText());
                        button.setName(textField.getText());
                        buttons.add(button);
                        button.addMouseListener(new MouseListener() {
                            @Override
                            public void mouseClicked(MouseEvent e) {

                            }

                            @Override
                            public void mousePressed(MouseEvent e) {

                            }

                            @Override
                            public void mouseReleased(MouseEvent e) {
                                if(e.getButton() == 3){
                                    buttons.remove(button);
                                    panel1.remove(button);
                                    panel1.revalidate();
                                    panel1.updateUI();
                                }
                            }

                            @Override
                            public void mouseEntered(MouseEvent e) {

                            }

                            @Override
                            public void mouseExited(MouseEvent e) {

                            }
                        });
                        filter.removeAll();
                        JPanel panel = new JPanel(new GridLayout(4,1));
                        panel.add(textField);
                        panel.add(addFilter);
                        panel.add(remove);
                        panel1.add(button);
                        filter.add(panel,BorderLayout.WEST);
                        filter.add(panel1,BorderLayout.EAST);
                        filter.revalidate();
                        filter.updateUI();
                        filter.repaint();
                    }
                }

            }
        };

        addFilter.addActionListener(actionListener);
        option1.addActionListener(actionListener);
        option2.addActionListener(actionListener);
        option3.addActionListener(actionListener);
        option4.addActionListener(actionListener);
        option5.addActionListener(actionListener);
        ButtonGroup group = new ButtonGroup();
        group.add(option1);
        group.add(option2);
        group.add(option3);
        group.add(option4);
        group.add(option5);

        JPanel mainpanel1 = new JPanel(new GridLayout(7,1));
        mainpanel1.add(option1);
        mainpanel1.add(option2);
        mainpanel1.add(option3);
        mainpanel1.add(option4);
        mainpanel1.add(option5);

        mainpanel1.setVisible(true);

        MyButton button = new MyButton("OK",setting,mainpanel1);
        button.getButton().setPreferredSize(new Dimension(40,40));
        mainpanel1.setBorder(new EmptyBorder(5,5,5,5));
        mainpanel1.add(button.getButton());

        JPanel main = new JPanel(new BorderLayout(5,5));
        chooserFrame.add(chooser);



        tab.add("File saver direction",chooserFrame);
        tab.add("set Look & Feel",mainpanel1);
        tab.add("filtering",filter);
        main.setBorder(new EmptyBorder(5,5,5,5));
        main.add(tab.getTabbedPane(),BorderLayout.CENTER);

        setting.setContentPane(main);
        setting.pack();
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public JFrame getSetting() {
        return setting;
    }

    public JPanel getFilter() {
        return filter;
    }

    public String getLookAndFeel() {
        return lookAndFeel;
    }




    public void setFilter(JPanel filter) {
        this.filter = filter;
    }

    public ArrayList<JButton> getButtons() {
        return buttons;
    }

    public void setL(ArrayList<String>strings) {
//        panel1 = new JPanel(new GridLayout(10,1));
//        buttons = new ArrayList<JButton>();
        for (int i=0;i<strings.size();i++){
            System.out.println("ss");
            JButton button = new JButton(strings.get(i));
            buttons.add(button);
            panel1.add(button);
            button.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {

                }

                @Override
                public void mousePressed(MouseEvent e) {

                }

                @Override
                public void mouseReleased(MouseEvent e) {
                if(e.getButton() == 3){
                    buttons.remove(button);
                    panel1.remove(button);
                    panel1.revalidate();
                    panel1.updateUI();
                }
                }

                @Override
                public void mouseEntered(MouseEvent e) {

                }

                @Override
                public void mouseExited(MouseEvent e) {

                }
            });
        }

        filter.add(panel1,BorderLayout.EAST);
        filter.revalidate();
        filter.updateUI();
        filter.repaint();
    }

    public void setLookAndFeel(String lookAndFeel) {
        this.lookAndFeel = lookAndFeel;
    }

    public void setSetting(JFrame setting) {
        this.setting = setting;
    }




}
