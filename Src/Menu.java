import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Menu {
    private JMenu menu;
    private JFrame currentFrame;
    public Menu(String in,JFrame frame){
        menu = new JMenu(in);
        currentFrame = frame;
        menu.setName(in);
    }
    public Menu(String in,int size,JFrame frame){
        Font font = new Font("DKM",25,size);
        menu = new JMenu(in);
        menu.setName(in);
        menu.setFont(font);
        currentFrame = frame;

    }
    public JMenu getMenu() {
        return menu;
    }
    public JMenu addMenuItem(MyMenuItem menuItem){
        menu.add(menuItem.getMenuItem());
        return menu;
    }
}
