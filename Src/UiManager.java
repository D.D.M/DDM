import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.*;
import java.io.Serializable;
import java.util.ArrayList;


public class UiManager implements Serializable{
    public static Frames frames;
    public static JFrame mainFrame;
    private static ArrayList<DownloadInfo> downloads;
    private static JPanel mainPanel;
    private static ArrayList<Menu> menu;
    private static ToolBar toolBar;
    private static ToolBar toolBarLeft;
    private static JPanel left;
    private static JPanel north;
    private static JPanel north2;
    private static Tab    tab;
    private static JPanel processing;
    private static JPanel defaulti;
    private static JPanel queues;
    private static JPanel completed;
    public static Queue queue;
    private static JTextField search;
    private static JScrollPane processingScrol;

    public UiManager(){
        //building main frame
        FileUtils fileUtils = new FileUtils();
        frames = new Frames();
        mainFrame = new JFrame("DDM");
        mainFrame.setName("main");
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setLocation(500,50);
        mainFrame.setMinimumSize(new Dimension(1000,1000));
        mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//        mainFrame.setPreferredSize(new Dimension(800,600));
        tab = new Tab();
        mainFrame.getRootPane().setBorder(BorderFactory.createMatteBorder(4, 4, 4, 4, Color.RED));

        //adding image icon
        ImageIcon imageIcon = new ImageIcon("Src\\index.png");
        mainFrame.setIconImage(imageIcon.getImage());

        //adding menu bar
        JMenuBar jMenuBar = new JMenuBar();
        //adding menu and menu items
        menu = new ArrayList<Menu>();
        menu.add(new Menu("DownloadInfo",25,mainFrame));
        menu.add(new Menu("Edit",25,mainFrame));
        menu.add(new Menu("About Us",20,mainFrame));
        menu.add(new Menu("Help",20,mainFrame));

        //adding Jmenu itemes
        menu.get(0).addMenuItem(new MyMenuItem("New DownloadInfo", KeyEvent.VK_F1, ActionEvent.ALT_MASK, KeyEvent.VK_N));
        menu.get(0).addMenuItem(new MyMenuItem("Resum", KeyEvent.VK_F2, ActionEvent.ALT_MASK,KeyEvent.VK_R));
        menu.get(0).addMenuItem(new MyMenuItem("Pause", KeyEvent.VK_F2, ActionEvent.ALT_MASK,KeyEvent.VK_P));
        menu.get(0).addMenuItem(new MyMenuItem("Cancel", KeyEvent.VK_F2, ActionEvent.ALT_MASK,KeyEvent.VK_C));
        menu.get(0).addMenuItem(new MyMenuItem("Remove", KeyEvent.VK_F2, ActionEvent.ALT_MASK,KeyEvent.VK_R));
        menu.get(0).addMenuItem(new MyMenuItem("Setting", KeyEvent.VK_F3, ActionEvent.ALT_MASK,KeyEvent.VK_S));
        menu.get(0).addMenuItem(new MyMenuItem("Exit", KeyEvent.VK_F4, ActionEvent.ALT_MASK,KeyEvent.VK_E));
        menu.get(2).addMenuItem(new MyMenuItem("About Us", KeyEvent.VK_A, ActionEvent.SHIFT_MASK,KeyEvent.VK_A));

        //adding menu in menu bar
        jMenuBar.add(menu.get(0).getMenu());
        jMenuBar.add(menu.get(1).getMenu());
        jMenuBar.add(menu.get(2).getMenu());
        jMenuBar.add(menu.get(3).getMenu());


        mainPanel = new JPanel(new BorderLayout(5,5));
        processing = new JPanel();
//        processing.setLayout(new BoxLayout(processing, BoxLayout.Y_AXIS));
//        SpringLayout springLayout = new SpringLayout();
        processing.setLayout(new GridLayout(11,1));

        queues = new JPanel(new BorderLayout(5,5));
        defaulti = new JPanel(new GridLayout(10,1));
        completed = new JPanel(new GridLayout(10,1));
        tab = new Tab();

        processingScrol = new JScrollPane(processing);
        processingScrol.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
//        processingScrol.setHorizontalScrollBar(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        processing.revalidate();
        processing.repaint();
        queue = new Queue(tab,processing,processingScrol);
        queues.add(tab.getTabbedPane(),BorderLayout.CENTER);
        queue.readFile();
        //left tool bar


        toolBarLeft = new ToolBar("vertical");
        toolBarLeft.addButton(new MyButton("Processing","Src\\processing.png","Processing",mainFrame));
        toolBarLeft.addButton(new MyButton("Completed","Src\\completed.png","Completed",mainFrame));
        toolBarLeft.addButton(new MyButton("Queues","Src\\queue.png","Queues",mainFrame));
        toolBarLeft.addButton(new MyButton("Default",mainFrame));
        //building main panel that is on main frame

       // tab.getTabbe
//        tab.add("default",defaulti);
//        tab.add("Queues","Src\\completed.png",queues);
//        tab.add("completed","Src\\queue.png",completed);
//        tab.add("Proccessing","Src\\processing.png",processing);

        upToolBar();

      //  mainPanel.setBackground(Color.blue);

        //building secondary panels to put toolbar in north with menu bar & same for left
        north = new JPanel(new BorderLayout(5,5));
        north2 = new  JPanel(new BorderLayout(5,5));
//        toolBar.getToolBar().addSeparator();

        //adding tool bar & menu bar
        defaulti.setBackground(Color.blue);
        left = new JPanel(new BorderLayout(5,5));
        JLabel lableLeft = new JLabel();
        lableLeft.setIcon(new ImageIcon("Src\\dkm0.jpg"));
//       left.add(tab.getTabbedPane(),BorderLayout.WEST);
//        left.add(lableLeft,BorderLayout.NORTH);
        north.add(lableLeft,BorderLayout.WEST);
        left.add(toolBarLeft.getToolBar(),BorderLayout.WEST);
//        tab.getTabbedPane().setPreferredSize(new Dimension(300,440));
//        tab.getTabbedPane().setMaximumSize(new Dimension(500,600));
//        tab.getTabbedPane().setTabPlacement(SwingConstants.LEFT);

        //tab.getTabbedPane().

//        north.add(lableLeft,BorderLayout.WEST);
        north2.add(toolBar.getToolBar(),BorderLayout.NORTH);
        north.add(jMenuBar,BorderLayout.NORTH);
        north.add(north2,BorderLayout.CENTER);

        mainPanel.add(left,BorderLayout.WEST);
        mainPanel.add(north,BorderLayout.NORTH);

        mainFrame.addKeyListener(new Key());

      //  JPanel panel = new JPanel();
       // panel.add(tab.getTabbedPane());
        mainFrame.setContentPane(mainPanel);
        systemTray();
        show(true);
    }
    public void show(boolean in){
       mainFrame.pack();
        mainFrame.setVisible(in);
    }

    public void systemTray(){
        SystemTray tray = SystemTray.getSystemTray();
        ImageIcon imageIcon = new ImageIcon("Src\\index.png");
        PopupMenu popup = new PopupMenu();

        java.awt.MenuItem exit = new java.awt.MenuItem("EXIT");
        exit.setName("EXIT");
        java.awt.MenuItem open = new java.awt.MenuItem("OPEN");
        open.setName("OPEN");
        ActionListener Listener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                if(e.getSource().equals(exit)) {
                        FileUtils.addDownload(queue.getDownloads());
                        FileUtils.saveSetting(frames.setting);
//                    FileUtils.save(Main.uiManager);
                    System.exit(0);
                }
                if(e.getSource().equals(open))
                    mainFrame.setVisible(true);
            }
        };
        exit.addActionListener(Listener);
        open.addActionListener(Listener);
        popup.add(exit);
        popup.add(open);
        TrayIcon trayIcon = new TrayIcon(imageIcon.getImage(), "DDM",popup);
        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }
    public static void Show(String in){
        if(in.equals("default")){
            defaulti.setVisible(true);
            mainPanel.remove(completed);
            mainPanel.remove(queues);
            mainPanel.remove(processingScrol);
            defaulti.setBackground(Color.YELLOW);
            mainPanel.add(defaulti,BorderLayout.CENTER);
        }
        if(in.equals("completed")){
            mainPanel.remove(defaulti);
            mainPanel.remove(queues);
            mainPanel.remove(processingScrol);
            completed.setVisible(true);
            mainPanel.add(completed,BorderLayout.CENTER);
        }
        if(in.equals("queues")){
            queues.setVisible(true);
            mainPanel.remove(defaulti);
            mainPanel.remove(completed);
            mainPanel.remove(processingScrol);
            mainPanel.add(queues,BorderLayout.CENTER);
        }
        if(in.equals("processing")){
            System.out.println("sad" + "");
            mainPanel.remove(defaulti);
            mainPanel.remove(completed);
            mainPanel.remove(queues);
            processing.setVisible(true);
            mainPanel.add(processingScrol,BorderLayout.CENTER);
        }
        mainFrame.setContentPane(mainPanel);
        LookAndFeel.updateLAFRecursively(mainFrame);
    }
    private class Key implements KeyListener{
        @Override
        public void keyTyped(KeyEvent e) {

        }

        @Override
        public void keyPressed(KeyEvent e) {

        }

        @Override
        public void keyReleased(KeyEvent e) {

        }
    }

    public static JPanel getProcessing() {
        return processing;
    }

    public static void setTab(Tab tab) {
        UiManager.tab = tab;
    }

    public static void setQueue(Queue queue) {
        UiManager.queue = queue;
    }

    public static JPanel getQueues() {
        return queues;
    }

    public static Tab getTab() {
        return tab;
    }

    public static Queue getQueue() {
        return queue;
    }

    public static void setCompleted(JPanel completed) {
        UiManager.completed = completed;
    }

    public static void setDefaulti(JPanel defaulti) {
        UiManager.defaulti = defaulti;
    }

    public static void setDownloads(ArrayList<DownloadInfo> downloads) {
        UiManager.downloads = downloads;
    }

    public static void setFrames(Frames frames) {
        UiManager.frames = frames;
    }

    public static void setLeft(JPanel left) {
        UiManager.left = left;
    }

    public static void setMainFrame(JFrame mainFrame) {
        UiManager.mainFrame = mainFrame;
    }

    public static void setMainPanel(JPanel mainPanel) {
        UiManager.mainPanel = mainPanel;
    }

    public static void setMenu(ArrayList<Menu> menu) {
        UiManager.menu = menu;
    }

    public static void setNorth(JPanel north) {
        UiManager.north = north;
    }

    public static void setNorth2(JPanel north2) {
        UiManager.north2 = north2;
    }

    public static void setProcessing(JPanel processing) {
        UiManager.processing = processing;
    }

    public static void setQueues(JPanel queues) {
        UiManager.queues = queues;
    }

    public static void setToolBar(ToolBar toolBar) {
        UiManager.toolBar = toolBar;
    }

    public static void setToolBarLeft(ToolBar toolBarLeft) {
        UiManager.toolBarLeft = toolBarLeft;
    }

    public static ArrayList<DownloadInfo> getDownloads() {
        return downloads;
    }

    public static ToolBar getToolBar() {
        return toolBar;
    }

    public static Frames getFrames() {
        return frames;
    }
    private void upToolBar(){
        search = new JTextField(20);

        //building toolbar and adding buttons
        toolBar = new ToolBar();
        toolBar.addButton(new MyButton("","Src\\add.png","New DownloadInfo",mainFrame));
        toolBar.addButton(new MyButton("","Src\\play.png","Resume",mainFrame));
        toolBar.addButton(new MyButton("","Src\\pause.png","pause",mainFrame));
        toolBar.getToolBar().addSeparator();
        toolBar.addButton(new MyButton("","Src\\stop.png","Remove",mainFrame));
        toolBar.addButton(new MyButton("","Src\\remove.png","cancel",mainFrame));
        toolBar.addButton(new MyButton("","Src\\sort.png","Sort",mainFrame));
        toolBar.addButton(new MyButton("","Src\\taskcleaner.png","removed",mainFrame));
        toolBar.addButton(new MyButton("","Src\\batchdownload.png","batchdownload",mainFrame));
        toolBar.addButton(new MyButton("add to zip",mainFrame));
        toolBar.getToolBar().addSeparator();
        search.setText("Type here what you want to search : ");
        // search.add(new JButton("OK"));

        search.setBorder(new TitledBorder(new LineBorder(Color.cyan, 2), "Search................"));
        search.setFocusable(true);
        JPanel panel = new JPanel(new BorderLayout(5,5));
        panel.add(search,BorderLayout.EAST);
        toolBar.getToolBar().add(panel);
        MouseListener mouseListener = new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {
            search.setText("");
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        };
        KeyListener keyListener = new KeyListener() {
            boolean flag = true;
            JPanel panel = new JPanel(new GridLayout(10, 1));
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {

                if((!search.getText().equals("Type here what you want to seach : ") && !search.getText().equals("")) || e.getKeyCode() != KeyEvent.VK_ENTER) {
                    String text = search.getText();
                    queue.search(text,panel);
                    if(flag){
                        flag = !flag;
                    }
                    mainPanel.remove(completed);
                    mainPanel.remove(queues);
                    mainPanel.remove(processingScrol);
                    mainPanel.remove(defaulti);
                    panel.setVisible(true);
                    mainPanel.add(panel,BorderLayout.CENTER);

                }
                else {
                    flag = !flag;
                    mainPanel.remove(panel);
                    mainPanel.add(processingScrol,BorderLayout.CENTER);
                    search.setText("Type here what you want to seach : ");
                }
                if(e.getKeyCode() == KeyEvent.VK_ENTER){
                    mainPanel.remove(panel);

                    search.setText("Type here what you want to seach : ");
                }
            }
        };
        search.addMouseListener(mouseListener);
        search.addKeyListener(keyListener);

    }

    public static ArrayList<Menu> getMenu() {
        return menu;
    }

    public static JFrame getMainFrame() {
        return mainFrame;
    }

    public static JPanel getCompleted() {
        return completed;
    }

    public static JPanel getDefaulti() {
        return defaulti;
    }

    public static JPanel getLeft() {
        return left;
    }

    public static JPanel getMainPanel() {
        return mainPanel;
    }

    public static JPanel getNorth() {
        return north;
    }

    public static JPanel getNorth2() {
        return north2;
    }

    public static JScrollPane getProcessingScrol() {
        return processingScrol;
    }

    public static JTextField getSearch() {
        return search;
    }

    public static ToolBar getToolBarLeft() {
        return toolBarLeft;
    }

}
//            if(KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner().equals(search)){
//
//                    }
