import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class NewDownload {
    private JFrame newDownload;
    private TextField Name;
    private TextField url;
    private String URL;
    private String name;
    private String address;
    private Image urlImage,nameImage;
    private  JTextField path;
    private  JComboBox comboBox ;
    private ArrayList<String> comboes;
    public  NewDownload(){
        //TODO download name and existed file
        try {
            address = UiManager.frames.setting.getAddress();
        }
        catch (NullPointerException e){
            System.out.println(e);
        }
        JPanel mainPanel = new JPanel(new BorderLayout(5,5));
        newDownload = new JFrame("New DownloadInfo");
        newDownload.setName("New DownloadInfo");
        JPanel textPanel = new JPanel(new GridLayout(4,2,5,5));
        JLabel label = new JLabel("   file name : ");
        label.setHorizontalAlignment(SwingConstants.CENTER);
        newDownload.setResizable(false);
        newDownload.setMinimumSize(new Dimension(400,200));
        JLabel label1 = new JLabel();
        label1.setBackground(Color.blue);
         Name = new TextField("Type here your FILE NAME","name",newDownload);
         url = new TextField("Type here your URL","URL",newDownload);
         urlImage = Toolkit.getDefaultToolkit().getImage("D:\\IdeaProjects\\DownloadManager\\DDM\\DDM\\Src\\url-link.png");
         JLabel urlLabel = new JLabel("  URL  :");
         urlLabel.setIcon(new ImageIcon("D:\\IdeaProjects\\DownloadManager\\DDM\\DDM\\Src\\url-link.png"));
        urlLabel.setHorizontalAlignment(SwingConstants.CENTER);
        JButton ok = new JButton("OK");
        int height = ok.getPreferredSize().height;
        int width = ok.getPreferredSize().width-10;
        ok.setPreferredSize(new Dimension(width,height));
        JButton cancel = new JButton("cancel");

         path = new JTextField();
        MyButton setPath = new MyButton("change",newDownload);
        JPanel panel = new JPanel(new GridLayout(2,4));
        JLabel label2 = new JLabel("Destination : ");
        label2.setHorizontalAlignment(SwingConstants.CENTER);
        panel.add(label2);
        panel.add(path);
        panel.add(setPath.getButton());

        DownloadInfo download = new DownloadInfo();
        download.setQueue("Processing");
        ActionListener actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(e.getSource().equals(ok)){
                    boolean flag = false;
                    for (int i=0;i<UiManager.frames.setting.getButtons().size();i++){
                        if(getUrl().getTextField().getText().contains(UiManager.frames.setting.getButtons().get(i).getName())){
                            Object[] options = {"ok"};
                            JOptionPane.showOptionDialog(null, "sorry your request rejected because entered site is filter by program :(", "failed!", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[0]);
                            flag = true;
                        }
                    }
                    File file = new File(getAddress() + "//" + getName().getTextField().getText());
                    if(file.exists()) {
                        Object[] options = {"ok"};
                        JOptionPane.showOptionDialog(null, "sorry your request rejected because name is repeted (file with that name and path already exists) :(", "failed!", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[0]);
                        flag = true;
                    }
                    java.net.URL url1 = null;
                    try {
                        url1 = new URL(url.getTextField().getText());
                    } catch (MalformedURLException e1) {
                        e1.printStackTrace();
                    }
                    URLConnection urlConnection = null;
                    try {
                        urlConnection = url1.openConnection();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    if(urlConnection.getContentLength()  == -1) {
                        flag = true;
                        Object[] options = {"ok"};
                        JOptionPane.showOptionDialog(null, "sorry your request rejected because url you entered is not valid! :(", "failed!", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[0]);

                    }
                    if(!flag) {
                        if(getName().getTextField().getText() == "" || getName().getTextField().getText().equals("Type here your FILE NAME")){
                            getName().getTextField().setText(url1.getFile());
                        }
                        download.setAddress(getAddress());
                        download.setName(getName().getTextField().getText());
                        download.setUrl(getUrl().getTextField().getText());
                        download.setValue(0);
                        download.setUrl(url.getTextField().getText());
                        download.setSize(urlConnection.getContentLength());
                        Download downloading = new Download(download);
                        UiManager.queue.add(download.getQueue(), downloading);
                        LookAndFeel.updateLAFRecursively(UiManager.mainFrame);
                        newDownload.setVisible(false);
                        System.out.println(url1.getFile());
                        Object[] options = {"ok"};
                        JOptionPane.showOptionDialog(null, "SUCCESSFULY ADDED :)", "Information", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
                    }
                }
                if(comboBox.getSelectedItem().equals("add New Queue")){
                    JFrame frame = new JFrame("add your Queue");
                    frame.setName("add your Queue");
                    JTextField textField = new JTextField();
                    frame.setLayout(new GridLayout(3,1));
                    JButton button = new JButton("OK");
                    frame.add(textField);
                    frame.add(new JLabel());
                    frame.add(button);
                    frame.setVisible(true);
                    frame.pack();
                    ActionListener actionListener1 = new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(textField.getText() != null){
                                frame.setVisible(false);
                                download.setQueue(textField.getText());
                            }
                        }
                    };
                    button.addActionListener(actionListener1);
                }
                for (int i=0;i<comboes.size();i++){
                    if(comboBox.getSelectedItem().equals(comboes.get(i))){
                        download.setQueue(comboBox.getSelectedItem().toString());
                    }
                }
            }
        };
        ok.addActionListener(actionListener);
        comboBox = new JComboBox();
        comboBox.addActionListener(actionListener);
        panel.add(comboBox);
        panel.add(new JLabel());
        panel.add(new JLabel());
        panel.add(new JLabel());

        path.setText(address);
        textPanel.add(urlLabel);
        textPanel.add(url.getTextField());
        textPanel.add(label);
        textPanel.add(Name.getTextField());
        textPanel.add(ok);
        textPanel.add(cancel);
        textPanel.add(label1);
        mainPanel.setBorder(new EmptyBorder(5,5,5,5));
        mainPanel.add(textPanel,BorderLayout.NORTH);
        mainPanel.add(panel,BorderLayout.CENTER);
        newDownload.setPreferredSize(new Dimension(600,400));
        newDownload.setContentPane(mainPanel);
        newDownload.pack();
    }
    public NewDownload(ArrayList<String> strings){
        this();
        setComboBoxItems(strings);

    }
    public Image getNameImage() {
        return nameImage;
    }

    public Image getUrlImage() {
        return urlImage;
    }

    public TextField getName() {
        return Name;
    }

    public TextField getUrl() {
        return url;
    }
    public void changePath(){

        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int status = chooser.showOpenDialog(null);
        if (status == JFileChooser.APPROVE_OPTION) {
            address = chooser.getCurrentDirectory().toString();
            path.setText(address);
        }
    }

    public String getAddress() {
        return address;
    }

    public String getURL() {
        return URL;
    }
    public String Getname(){
        return name;
    }
    public JFrame getNewDownload() {
        return newDownload;
    }
    public void update(String in){
        comboBox.addItem(in);
        comboes.add(in);
    }
    public void setComboBoxItems(ArrayList<String>strings){
        comboes = strings;
        comboBox.addItem("default");
        if(strings.size() != 0) {
            for (int i = 0; i < strings.size(); i++) {
                comboBox.addItem(strings.get(i));
            }
        }
        comboBox.addItem("add New Queue");

    }
}
