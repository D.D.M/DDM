import com.sun.xml.internal.ws.developer.Serialization;

import java.io.File;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DownloadInfo implements Serializable {
    private String url;
    private String name;
    private String address;
    private String type;
    private String queue;
    private boolean removed;
    private float value;
    private boolean pause;
    private boolean running;
    private long readed;
    private String lastModified;
    private boolean complete;
    private boolean cancel;
    private float speed;
    private Date date;
    private int size;
    public DownloadInfo() {
        cancel = false;
        speed = 0;
        lastModified = null;
        pause = false;
        running = false;
        readed = 0;
        complete = false;
        removed = false;
//        DateFormat df;
//         df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
         date= new Date();
         Calendar calendar;
         calendar = Calendar.getInstance();
         date = calendar.getTime();
//        df.format(calobj.getTime())
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public Date getDate() {
        return date;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void reset(){
        removed = false;
        cancel = false;
        lastModified = null;
        pause = false;
        running = false;
        readed = 0;
        complete = false;
        value = 0;
    }
    public boolean isCancel() {
        return cancel;
    }

    public void setCancel(boolean cancel) {
        this.cancel = cancel;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public void setReaded(long readed) {
        this.readed = readed;
    }

    public long getReaded() {
        return readed;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getLastModified() {
        return lastModified;
    }

    public String getQueue() {
        return queue;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPause(boolean pause) {
        this.pause = pause;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public boolean isPause() {
        return pause;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }

    public boolean isRemoved() {
        return removed;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }
    public String getUrl() {
        return url;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public float getValue() {
        return value;
    }
}
