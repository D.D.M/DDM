import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

public class MyButton{
    private JButton button;
    private String name;
    private String father;
    private JFrame currentFrame;
    private JPanel panel;

    public MyButton(String name,JFrame cur){
        currentFrame = cur;
        buildButton(name);
        this.name = name;
        father = name;
    }

    public MyButton(String name,JFrame cur,JPanel panel){
        currentFrame = cur;
        buildButton(name);
        this.name = name;
        this.panel = panel;
        father = name;
    }
    public MyButton (String name,String imageIconAdress,String iconName,JFrame cur){
        currentFrame = cur;
        buildButton(name,imageIconAdress, iconName);
        this.name = iconName;
        father = name;
//        button.setSize();
    }
    public MyButton (String name,String imageIconAdress,String iconName,JFrame cur,JPanel panel){
        currentFrame = cur;
        buildButton(name,imageIconAdress, iconName);
        this.name = iconName;
        father = name;
        this.panel = panel;
//        button.setSize();
    }
    public void setButton(JButton button) {
        this.button = button;
    }
    public void buildButton(String name,String imageIconAdress,String iconName){
        if(!name.equals("")) {
            button = new JButton(name);
            button.addActionListener(new Action());
        }
        else {
            button = new JButton();
            button.addActionListener(new Action());
        }
        button.setName(iconName);
        button.setIcon(new ImageIcon(imageIconAdress));
    }
    public void buildButton(String name,String iconName){
        button = new JButton(name);
        button.setName(iconName);
        button.addActionListener(new Action());
    }
    public void buildButton(String name){
        button = new JButton(name);
        button.setName(name);
        button.addActionListener(new Action());
    }
    public JButton getButton() {
        return button;
    }
    private class Action implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println(currentFrame.getName());
            if(name.equals("New DownloadInfo") && currentFrame.getName().equals("main")){
                NewDownload newDownload = new NewDownload();
                UiManager.frames.newDownload = newDownload;
                newDownload.setComboBoxItems(UiManager.queue.getStrings());
                UiManager.frames.newDownload.getNewDownload().setVisible(true);
            }
            if(name.equals("cancel") && currentFrame.getName().equals("New DownloadInfo")){
                currentFrame.setVisible(false);
                Object[] options = {"ok"};
                JOptionPane.showOptionDialog(null, "DownloadInfo canceld!!!", "Attention", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
            }
            if((name.equals("Edit") || name.equals("Setting"))&& currentFrame.getName().equals("main")){
                UiManager.frames.setting.getSetting().setVisible(true);
            }
            if((name.equals("OK") || name.equals("cancel"))&& currentFrame.getName().equals("ABOUT US")){
                UiManager.frames.getAbout().setVisible(false);
            }
            if(name.equals("OK") && currentFrame.getName().equals("Setting")){
                UiManager.frames.setting.getSetting().setVisible(false);
            }
            if(name.equals("Cancel") && currentFrame.getName().equals("Setting")){
                UiManager.frames.setting.getSetting().setVisible(false);
            }
            if(name.equals("OK") && currentFrame.getName().equals("Information")) {
                currentFrame.setVisible(false);
            }
            if(name.equals("change") && currentFrame.getName().equals("New DownloadInfo")){
            UiManager.frames.newDownload.changePath();
        }
            if(name.equals("Processing") && currentFrame.getName().equals("main")){
                UiManager.Show("processing");
            }
            if(name.equals("Default") && currentFrame.getName().equals("main")){
                UiManager.Show("default");
            }
            if(name.equals("Completed") && currentFrame.getName().equals("main")){
                UiManager.Show("completed");
            }
            if(name.equals("Queues") && currentFrame.getName().equals("main")){
                UiManager.Show("queues");
            }
            if(name.equals("batchdownload") && currentFrame.getName().equals("main")){
            UiManager.frames.queueFrame();
        }
            if(name.equals("pause") && currentFrame.getName().equals("main")){
          for(int i=0;i<UiManager.queue.getDownloads().size();i++){
              if(UiManager.queue.getDownloads().get(i).getDownloadInfo().isRunning())
                  UiManager.queue.getDownloads().get(i).getDownloadInfo().setPause(true);
              UiManager.queue.setCount(UiManager.queue.getCount()-1);
          }
            }
            if(name.equals("Resume") && currentFrame.getName().equals("main")){
                ArrayList<Download> downloads = UiManager.queue.getDownloads();
                ArrayList<ProgressBar> progressBars = UiManager.queue.getProgressBars();
                for (int i = 0;i<progressBars.size();i++){
                    if(progressBars.get(i).isSelected() && progressBars.get(i).getQueue().equals("Processing")){
                      for (int j=0;j<downloads.size();j++){
                          for (int k=0;k < downloads.get(j).getProgressBar().size();k++){
                              if(downloads.get(j).getProgressBar().get(k) == progressBars.get(i)){
                                  if(downloads.get(j).getDownloadInfo().isRunning()) {  //CHECK TODO
                                  File file = downloads.get(j).getDownloadFile();
                                  ArrayList<ProgressBar>progressBars1 = downloads.get(j).getProgressBar();
                                  DownloadInfo downloadInfo = downloads.get(j).getDownloadInfo();
                                  if(downloadInfo.isCancel()) {
                                      downloadInfo.reset();
                                      downloads.set(j, new Download());
                                      for (int l = 0;l<progressBars1.size();l++)
                                     progressBars.get(i).setValue(0);
                                      downloads.get(j).setDownloadInfo(downloadInfo);
//                                      UiManager.queue.add(downloads.get(j).getDownloadInfo().getQueue(),downloads.get(j));
                                      downloads.get(j).getDownloadInfo().setCancel(false);
                                      downloads.get(j).getDownloadInfo().setPause(false);
                                      downloads.get(j).setNewFile();
                                      downloads.get(j).setProgressBar(progressBars1);
                                      downloads.get(j).execute();
                                  }
//                                  if(downloadInfo.isPause()){
//                                      downloads.set(j, new Download());
//                                      downloads.get(j).setDownloadFile(file);
//                                      downloads.get(j).setDownloadInfo(downloadInfo);
//                                      downloads.get(j).getDownloadInfo().setCancel(false);
//                                      downloads.get(j).getDownloadInfo().setPause(false);
//                                      downloads.get(j).setProgressBar(progressBars1);
//                                      downloads.get(j).execute();
//                                  }
                                      downloads.get(j).getDownloadInfo().setCancel(false);
                                      downloads.get(j).getDownloadInfo().setPause(false);
                                      downloads.get(j).execute();
                                  }
                                  else if(UiManager.queue.getCount() <= UiManager.queue.getHamzaman()){
                                      downloads.get(j).getDownloadInfo().setCancel(false);
                                      downloads.get(j).getDownloadInfo().setPause(false);
                                      if(!downloads.get(j).getDownloadInfo().isPause())
                                      downloads.get(j).execute();
                                  }

//                                  downloads.get(j).getDownloadInfo().setCancel(false);
//                                  downloads.get(j).getDownloadInfo().setPause(false);
//                                  downloads.get(j).execute();

//  else {
//                                  }
//                                  e/lse {
//                                      downloads.get(j).getDownloadInfo().setPause(false);
//                                  }
                              }
                          }
                      }
                    }
                }
            }
            if(name.equals("add to zip") && currentFrame.getName().equals("main")){
                ArrayList<ProgressBar> progressBars = UiManager.queue.getProgressBars();
                ArrayList<Download> downloads = UiManager.queue.getDownloads();
                for (int i = 0;i<progressBars.size();i++){
                    if(progressBars.get(i).isSelected()){
                       for (int j=0;j<downloads.size();j++){
                           if(downloads.get(j).getProgressBar().contains(progressBars.get(i))){
                               UiManager.queue.addToZip(downloads.get(j));
                               break;
                           }
                       }
                    }
                }
            }
            if(name.equals("Remove") && currentFrame.getName().equals("main")){
                ArrayList<ProgressBar> progressBars = UiManager.queue.getProgressBars();
                ArrayList<Download> downloads = UiManager.queue.getDownloads();
                for (int i = 0;i<progressBars.size();i++){
                    if(progressBars.get(i).isSelected()){
                        progressBars.get(i).getDownload().getDownloadInfo().setRemoved(true);
                        System.out.println("removed");
                    }
                }
               UiManager.getProcessing().removeAll();
                UiManager.getQueues().removeAll();
                Tab tab = new Tab();
                Queue queue = new Queue(tab,UiManager.getProcessing(),UiManager.getProcessingScrol());
                UiManager.setQueue(queue);
                UiManager.setTab(tab);
                UiManager.getQueues().add(tab.getTabbedPane(), BorderLayout.CENTER);
                for (int i=0;i<downloads.size();i++){
                    queue.add(downloads.get(i).getDownloadInfo().getQueue(),downloads.get(i));
                }
            }
            if(name.equals("cancel") && currentFrame.getName().equals("main")){
                ArrayList<ProgressBar> progressBars = UiManager.queue.getProgressBars();
                ArrayList<Download> downloads = UiManager.queue.getDownloads();
                for (int i = 0;i<progressBars.size();i++){
                    if(progressBars.get(i).isSelected()){
                        progressBars.get(i).getDownload().getDownloadInfo().setCancel(true);
                        System.out.println("cancel");
                    }
                }
            }
            if(name.equals("Sort") && currentFrame.getName().equals("main")){
                JFrame frame = new JFrame("SORTING....");
                    JPanel panel = new JPanel(new GridLayout(4,2));
                    JRadioButton option1 = new JRadioButton("calender");
                    JRadioButton option2 = new JRadioButton("size");
                    JRadioButton option3 = new JRadioButton("name");
                    JRadioButton option4 = new JRadioButton("...");
                    ButtonGroup buttonGroup = new ButtonGroup();
                    buttonGroup.add(option1);
                    buttonGroup.add(option2);
                    buttonGroup.add(option3);
                    buttonGroup.add(option4);
                    JRadioButton o2 = new JRadioButton("incriseing");
                    JRadioButton o1 = new JRadioButton("decrizing");
                    ButtonGroup buttonGroup1 = new ButtonGroup();
                    buttonGroup1.add(o1);
                    buttonGroup1.add(o2);
                    panel.add(option1);
                    panel.add(option2);
                    panel.add(option3);
                    panel.add(option4);
                    panel.add(o1);
                    panel.add(o2);
                    frame.setContentPane(panel);
                    frame.pack();
                    frame.setVisible(true);
                    ActionListener actionListener = new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(e.getSource().equals(option3)){
                                System.out.println("asf");
                                UiManager.queue.setSort("name");
                        }
                        if(e.getSource().equals(option2)){
                                UiManager.queue.setSort("size");
                        }
                            if(e.getSource().equals(option1)){
                                UiManager.queue.setSort("calender");
                            }
                        if(e.getSource().equals(o1)){
                                UiManager.queue.setSortnig(true);
                        }
                        if(e.getSource().equals(o2)){
                                UiManager.queue.setSortnig(false);
                        }
                    }
                };
                    option1.addActionListener(actionListener);
                    option2.addActionListener(actionListener);
                    option3.addActionListener(actionListener);
                    option4.addActionListener(actionListener);
            }
            if(name.equals("removed") && currentFrame.getName().equals("main")){
                UiManager.queue.removed();
            }
        }
    }
    }

