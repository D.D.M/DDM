import javax.net.ssl.HttpsURLConnection;
import javax.swing.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Download extends SwingWorker<Integer, Integer> {
    private DownloadInfo downloadInfo;
    private File downloadFile;
    private ArrayList<ProgressBar> progressBar;
    private boolean queue;



    public Download(DownloadInfo downloadInfo) {
        queue = false;
        this.downloadInfo = downloadInfo;
        downloadFile = new File(downloadInfo.getAddress() + "\\" + downloadInfo.getName());
        progressBar = new ArrayList<ProgressBar>();
    }

    public Download() {
    }

    public void setQueue(boolean queue) {
        this.queue = queue;
    }

    public boolean isQueue() {
        return queue;
    }

    public void setProgressBar(ArrayList<ProgressBar> progressBar) {
        this.progressBar = progressBar;
    }
    public void setNewFile(){
    downloadFile = new File(downloadInfo.getAddress() + "\\" + downloadInfo.getName());
    }
    public void setDownloadInfo(DownloadInfo downloadInfo) {
        this.downloadInfo = downloadInfo;
    }

    public void setDownloadFile(File downloadFile) {
        this.downloadFile = downloadFile;
    }

    public void addProgressBar(ProgressBar progressBar) {
        this.progressBar.add(progressBar);
    }

    public DownloadInfo getDownloadInfo() {
        return downloadInfo;
    }

    public File getDownloadFile() {
        return downloadFile;
    }

    @Override
    protected Integer doInBackground() throws Exception {
        System.out.println("hey!");




        byte data[] = new byte[1024];
        int count;
        long readed = 0;
        if (downloadFile.exists()) {
            readed = (long) downloadFile.length();
        }
        URL url = new URL(downloadInfo.getUrl());
        HttpURLConnection urlConnection;
        System.out.println(url.getProtocol());
        if (url.getProtocol().equals("http")) {
            urlConnection = (HttpURLConnection) url.openConnection();
        } else if (url.getProtocol().equals("https")) {
            urlConnection = (HttpsURLConnection) url.openConnection();
        } else return 0;
//        urlConnection.setConnectTimeout(14000);
        long size = (urlConnection.getContentLengthLong());
        System.out.println(size);
        if (readed != 0) {

//            urlConnection.setRequestProperty("Range", "bytes=" + readed + "-" + downloadFile.length());

//            System.out.println(url.getUserInfo());
//            System.out.println(url.toExternalForm());
//            System.out.println(url.getAuthority());
//            System.out.println(url.getPath());
//            System.out.println(url.getRef());
//            System.out.println(url.getQuery());
//            urlConnection.getPermission();
            System.out.println(downloadFile.length());


//            urlConnection.setRequestProperty("Range", "bytes=" + downloadFile.length() + "-" + size);
//            urlConnection.setRequestMethod("IF-Range",downloadInfo.getLastModified().toString());
            if(downloadInfo.getLastModified() != null)
            urlConnection.setRequestMethod(downloadInfo.getLastModified());
            System.out.println("dasdas");
//            BufferedInputStream in = new BufferedInputStream(urlConnection.getInputStream());
        }
//        urlConnection.connect();
        float c = readed;
        //TODO

        boolean flag = false;
        long sec = System.currentTimeMillis();
        long finalSec = 0;
        int repeat = 0;
        long timeReaded = 0;
        System.out.println("0");
//        try (BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(downloadFile))) {
          try (FileOutputStream out = new FileOutputStream(downloadFile,true)){
            downloadInfo.setRunning(true);
//            in.skip(downloadInfo.getReaded());
            UiManager.queue.setCount(UiManager.queue.getCount() + 1);
            while (!downloadInfo.isCancel() && !downloadInfo.isRemoved() && readed < size) {
                System.out.println("1");
                while (urlConnection.getHeaderField("Location") != null) {
                    System.out.println("2");
                    url = new URL(downloadInfo.getUrl());
                    switch (url.getProtocol()) {
                        case "http":
                            urlConnection = (HttpURLConnection) url.openConnection();
                            break;
                        case "https":
                            urlConnection = (HttpsURLConnection) url.openConnection();
                            break;
                        default:
                            return 0;
                    }

                        urlConnection.setRequestProperty("Range", "bytes=" + downloadFile.length() + "-");
                    urlConnection.connect();
                }

//                url.openConnection();
                BufferedInputStream in = new BufferedInputStream(urlConnection.getInputStream());
//                BufferedInputStream in = new BufferedInputStream(url.openStream());
                while ((count = in.read(data, 0, 1024)) != -1 && !downloadInfo.isCancel() && !downloadInfo.isRemoved()) {
//                System.out.println("ss");
                    if (repeat == 0)
                        finalSec = System.currentTimeMillis();
//                System.out.println("s");
//                while (downloadInfo.isPause()){
//                    Thread.sleep(1000);
//                }

                    c += count;
                    readed += count;
                    //downloadInfo.setValue(count + downloadInfo.getValue());
                    float d = c / (float) size * 100;
//                System.out.println(d);
                    setProcess(d, readed);
                    out.write(data, 0, count);
//                    out.write(Paths.get(downloadFile.getPath()), data, StandardOpenOption.APPEND);
                    repeat++;
                    timeReaded += count;
                    if (repeat == 40) {
                        sec = System.currentTimeMillis();
                        float speed;
                        speed = ((float) timeReaded) / (((float) (sec - finalSec)));
//                    downloadInfo.setSpeed(speed);
                        for (int i = 0; i < progressBar.size(); i++) {
                            progressBar.get(i).updateLable(speed);
                        }
                        timeReaded = 0;
                        repeat = 0;
                    }
                    int a=0;
                    while (downloadInfo.isPause()) {
                        if(a==0) {
                            String lastModified = urlConnection.getHeaderField("Last-Modified");
                            downloadInfo.setLastModified("" + urlConnection.getHeaderFieldLong("Last-Modified", downloadInfo.getReaded()));
                        }
//                            in.close();
//                            return 0;
                        flag = true;
                        Thread.sleep(4000);
                        a++;
                    }
                    if (flag) {
                            System.out.println("2");
                            url = new URL(downloadInfo.getUrl());
                            if ("http".equals(url.getProtocol())) {
                                urlConnection = (HttpURLConnection) url.openConnection();

                            } else if ("https".equals(url.getProtocol())) {
                                urlConnection = (HttpsURLConnection) url.openConnection();

                            } else {
                                return 0;
                            }
                            System.out.println("ay khedaa");
                            urlConnection.setRequestProperty("Range", "bytes=" + downloadFile.length() + "-" + size);
                            System.out.println("daaaaaaaaaaaam");
                            urlConnection.connect();
                         in = new BufferedInputStream(urlConnection.getInputStream());
                        flag = false;
                    }
                }
            }
            if (readed != size) {
                String lastModified = urlConnection.getHeaderField("Last-Modified");
                downloadInfo.setLastModified(lastModified);
            }
            if (readed == size)
                downloadInfo.setComplete(true);
            return 100;
        }
    }



    //        try (RandomAccessFile out = new RandomAccessFile(downloadFile, "rw")) {
//            downloadInfo.setRunning(true);
//            out.seek(readed);
//            while ((count = in.read(data, 0, 1024)) != -1) {
//                if (downloadInfo.isPause()) {
//                    String lastModified = urlConnection.getHeaderField("Last-Modified");
//                    downloadInfo.setLastModified(lastModified);
//                    in.close();
//                    return 0;
//                }
//                c += count;
//                readed += count;
//                float d = c / (float) size * 100;
//                System.out.println(d);
//                setProcess(d, readed);
//                out.write(data, 0, count);
//            }
//        }
    //    @Override
//    protected void process(int chunks) {
//        for (int i = 0; i < progressBar.size(); i++) {
//            progressBar.get(i).setValue(chunks);
//        }
//    }
    public void setProcess(Float c, long readed) {
        c.intValue();
        for (int i = 0; i < progressBar.size(); i++) {
            progressBar.get(i).setValue(c.intValue());
        }
        downloadInfo.setReaded(readed);
        downloadInfo.setValue(c.intValue());
    }


    @Override
    protected void done() {
        System.out.println("fasfasfafa");
        if(downloadInfo.isCancel()){
            downloadFile.delete();
            downloadInfo.setLastModified(null);
            UiManager.queue.setCount(UiManager.queue.getCount()-1);
            return;
        }
        if (downloadInfo.isPause()) {
            return;
        }
        for (int i = 0; i < progressBar.size(); i++) {
            if(downloadInfo.isComplete()) {
                progressBar.get(i).setValue(100);
                downloadInfo.setValue(100);
            }
        }
        if(downloadInfo.isComplete()) {
            ProgressBar progressBar = new ProgressBar(this);
            UiManager.getCompleted().add(progressBar.getPanel());
            downloadInfo.setRunning(false);
            UiManager.queue.setCount(UiManager.queue.getCount()-1);
        }
        //TODO
//        if (downloadInfo.getQueue().equals("Processing")) {
//            System.out.println(progressBar.get(0).getValue());
//        if(progressBar.get(0).getValue() != 100)
//            UiManager.queue.startQueue(downloadInfo.getQueue());
//        }
        if(queue){
            UiManager.queue.startQueue(downloadInfo.getQueue());
        }
    }

    public ArrayList<ProgressBar> getProgressBar() {
        return progressBar;
    }
}





